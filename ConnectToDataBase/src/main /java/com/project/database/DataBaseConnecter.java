package com.project.database;

/**
 * 
 * @author qaisazzeh
 *
 */
public class DataBaseConnecter {

	private String dataBaseConnectionUrl;
	private String dataBaseUser;
	private String dataBasePassword;

	public DataBaseConnecter(String dataBaseConnectionUrl, String dataBaseUser, String dataBasePassword) {
		this.dataBaseConnectionUrl = dataBaseConnectionUrl;
		this.dataBaseUser = dataBaseUser;
		this.dataBasePassword = dataBasePassword;
	}

	public String getDataBaseConnectionUrl() {
		return dataBaseConnectionUrl;
	}

	public void setDataBaseConnectionUrl(String dataBaseConnectionUrl) {
		this.dataBaseConnectionUrl = dataBaseConnectionUrl;
	}

	public String getDataBaseUser() {
		return dataBaseUser;
	}

	public void setDataBaseUser(String dataBaseUser) {
		this.dataBaseUser = dataBaseUser;
	}

	public String getDataBasePassword() {
		return dataBasePassword;
	}

	public void setDataBasePassword(String dataBasePassword) {
		this.dataBasePassword = dataBasePassword;
	}

	@Override
	public String toString() {
		return "DataBaseConnecter [dataBaseConnectionUrl=" + dataBaseConnectionUrl + ", dataBaseUser=" + dataBaseUser
				+ ", dataBasePassword=" + dataBasePassword + "]";
	}

}
