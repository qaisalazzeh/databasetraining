package com.project.database.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * @author qaisazzeh
 *
 */
public class CreditCardTransaction implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private long id;
	private long orderId;
	private Date creationDate;
	private Date updatedDate;
	private String transactionStatus;
	private Long statusNumber;
	private Long creditCardRequestId;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getOrderId() {
		return orderId;
	}

	public void setOrderId(long orderId) {
		this.orderId = orderId;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getTransactionStatus() {
		return transactionStatus;
	}

	public void setTransactionStatus(String transactionStatus) {
		this.transactionStatus = transactionStatus;
	}

	public Long getStatusNumber() {
		return statusNumber;
	}

	public void setStatusNumber(Long statusNumber) {
		this.statusNumber = statusNumber;
	}

	public Long getCreditCardRequestId() {
		return creditCardRequestId;
	}

	public void setCreditCardRequestId(Long creditCardRequestId) {
		this.creditCardRequestId = creditCardRequestId;
	}

	@Override
	public String toString() {
		return "CreditCardTransaction [id=" + id + ", orderId=" + orderId + ", creationDate=" + creationDate
				+ ", updatedDate=" + updatedDate + ", transactionStatus=" + transactionStatus + ", statusNumber="
				+ statusNumber + ", creditCardRequestId=" + creditCardRequestId + "]";
	}

}
