package com.project.database.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * @author qaisazzeh
 *
 */
public class CreditCardRequest implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private long id;
	private String requestName;
	private String requestDescreption;
	private Date creationDate;
	private Date updatedDate;
	private String requestStatus;
	private Long statusNumber;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getRequestName() {
		return requestName;
	}

	public void setRequestName(String requestName) {
		this.requestName = requestName;
	}

	public String getRequestDescreption() {
		return requestDescreption;
	}

	public void setRequestDescreption(String requestDescreption) {
		this.requestDescreption = requestDescreption;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getRequestStatus() {
		return requestStatus;
	}

	public void setRequestStatus(String requestStatus) {
		this.requestStatus = requestStatus;
	}

	public Long getStatusNumber() {
		return statusNumber;
	}

	public void setStatusNumber(Long statusNumber) {
		this.statusNumber = statusNumber;
	}

	@Override
	public String toString() {
		return "CreditCardRequest [id=" + id + ", requestName=" + requestName + ", requestDescreption="
				+ requestDescreption + ", creationDate=" + creationDate + ", updatedDate=" + updatedDate
				+ ", requestStatus=" + requestStatus + ", statusNumber=" + statusNumber + "]";
	}
	
	

}
