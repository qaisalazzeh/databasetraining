package com.project.database.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * @author qaisazzeh
 *
 */
public class Order implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private long id;
	private String orderName;
	private String orderDescreption;
	private Date creationDate;
	private Date updatedDate;
	private String orderStatus;
	private Long statusNumber;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getOrderName() {
		return orderName;
	}

	public void setOrderName(String orderName) {
		this.orderName = orderName;
	}

	public String getOrderDescreption() {
		return orderDescreption;
	}

	public void setOrderDescreption(String orderDescreption) {
		this.orderDescreption = orderDescreption;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}

	public Long getStatusNumber() {
		return statusNumber;
	}

	public void setStatusNumber(Long statusNumber) {
		this.statusNumber = statusNumber;
	}

	@Override
	public String toString() {
		return "Order [id=" + id + ", orderName=" + orderName + ", orderDescreption=" + orderDescreption
				+ ", creationDate=" + creationDate + ", updatedDate=" + updatedDate + ", orderStatus=" + orderStatus
				+ ", statusNumber=" + statusNumber + "]";
	}

}
