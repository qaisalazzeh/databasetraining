package com.project.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import com.project.database.entity.Order;
import com.project.services.OrderServiceLayer;

/**
 * 
 * @author qaisazzeh
 *
 */
public class MainClass {

	public static final String JDBC_CLASS_NAME = "com.mysql.jdbc.Driver";

	public static void main(String[] args) {

		// create new Object with Data needed to Connect to database
		DataBaseConnecter dataBaseConnecter = new DataBaseConnecter(
				"jdbc:mysql://127.0.0.1:3306/TestDatabase?autoReconnect=true&useSSL=false", "root", "rootroot");

		try {
			// here will opent new Connection With Data Base
			Connection dataBaseConnection = connectToDataBase(dataBaseConnecter);

			// will create new Order And Save it into DataBase
			OrderServiceLayer orderServiceLayer = new OrderServiceLayer();
			Order order = orderServiceLayer.createNewOrderObject(3, "Order2", "MacBook2", "APPROVED", 1);
			orderServiceLayer.saveOrderIntoDataBase(order, dataBaseConnection);
			List<Order> orderList = orderServiceLayer.getAllOrders(dataBaseConnection);
			System.out.println(orderList);

			// Will create new Request And Save it into DataBase

			// Will create new Transaction And Save it into Database

			closeDatabaseConnection(dataBaseConnection);

			// ResultSet rs = stmt.executeQuery("select * from emp");
			// while (rs.next())
			// System.out.println(rs.getInt(1) + " " + rs.getString(2) + " " +
			// rs.getString(3));

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private static Connection connectToDataBase(DataBaseConnecter dataBaseConnecter) throws Exception {
		Class.forName(JDBC_CLASS_NAME);
		Connection connection = DriverManager.getConnection(dataBaseConnecter.getDataBaseConnectionUrl(),
				dataBaseConnecter.getDataBaseUser(), dataBaseConnecter.getDataBasePassword());
		return connection;

	}

	private static void closeDatabaseConnection(Connection dataBaseConnection) throws SQLException {
		dataBaseConnection.close();
	}

}
