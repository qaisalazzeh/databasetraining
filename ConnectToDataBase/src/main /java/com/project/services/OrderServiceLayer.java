package com.project.services;
/**
 * 
 * @author qaisazzeh
 *
 */

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.project.database.entity.Order;

public class OrderServiceLayer {

	public Order createNewOrderObject(long Id, String orderName, String orderDescreption, String orderStatus,
			long statusNumber) {
		Order order = new Order();
		order.setId(Id);
		order.setOrderName(orderName);
		order.setOrderDescreption(orderDescreption);
		order.setOrderStatus("APPROVED");
		order.setStatusNumber(1L);
		order.setCreationDate(new Date());
		order.setUpdatedDate(null);
		return order;
	}

	public Order saveOrderIntoDataBase(Order order, Connection dataBaseConnection) throws SQLException {

		Statement saveStatement = dataBaseConnection.createStatement();
		StringBuilder saveOrderQuery = new StringBuilder();
		saveOrderQuery.append("INSERT INTO `TestDatabase`.`Order`");
		saveOrderQuery.append(" (`id`, `orderName`, `orderDescreption`, `orderStatus`, `statusNumber`) ");
		saveOrderQuery.append(" VALUES (");
		saveOrderQuery.append(order.getId() + " ,");
		saveOrderQuery.append("\'" + order.getOrderName() + "\',");
		saveOrderQuery.append("\'" + order.getOrderDescreption() + "\',");
		saveOrderQuery.append("\'" + order.getOrderStatus() + "\',");
		saveOrderQuery.append(order.getStatusNumber() + " )");

		System.out.println("in save Order to Database Query ====>" + saveOrderQuery.toString());

		saveStatement.execute(saveOrderQuery.toString());

		return order;

	}

	public List<Order> getAllOrders(Connection dataBaseConnection) throws SQLException {
		String getAllOrdersQuery = "select * from `testdatabase`.`Order`";
		List<Order> orderList = new ArrayList<>();
		Statement saveStatement = dataBaseConnection.createStatement();
		ResultSet executeQuery = saveStatement.executeQuery(getAllOrdersQuery);
		while (executeQuery.next()) {
			System.out.println(
					executeQuery.getString(1) + "  " + executeQuery.getString(2) + "  " + executeQuery.getString(3));
		}

		return orderList;
	}

}
